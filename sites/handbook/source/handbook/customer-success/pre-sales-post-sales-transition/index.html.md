---
layout: handbook-page-toc
title: Transitioning Accounts from Pre-Sales to Post-Sales
description: >-
  How to effectively transition a customer from the pre-sales engagement to post-sales to ensure the customer is successful.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

When a prospect is moving towards becoming a customer, they will need to be transitioned from the pre-sales engagement model to post-sales. In practice, that means that the Solutions Architect works with the Technical Account Manager to facilitate a smooth handoff and ensure continuity for the customer.

## Transition timeline

Once a prospect opportunity reaches [stage `5-Negotiating`](/handbook/sales/field-operations/gtm-resources/#opportunity-stages), the SA and TAM should begin the transition process. This includes the steps outlined in the sections below.

Most of the steps outlined below should occur between the opportunity reaching stage `5-Negotiating` and `Closed Won`.

### Internal account team discussion

In preparation for introducing the TAM, the [account team](/handbook/customer-success/account-team/) should schedule a meeting to discuss general information about the customer, to include:

- Account name and industry/sector
- Key people
  - Champion
  - Decision maker/economic buyer
  - Main points of contact
- Command Plan and reasons for purchasing GitLab
  - Defined business outcomes
  - Pain points
  - Metrics
  - Impediments to adoption and known risks
- POV overview
  - Technical objectives and results
  - Pending items to resolve
  - Customer's required capabilities
  - Overall sentiment coming out of the POV
- Services engagement
  - Scope and deliverables
  - Timeline

If the Solutions Architect created a collaboration project, the TAM and SA should review it together, and ensure that `readme` details and issues are up to date.

### TAM introduction to the customer

At the conclusion of the POV, the Solutions Architect conducts a review meeting with the customer. During this meeting the TAM should be introduced, and provide the customer an overview of the TAM role and what their engagement will look like going forward.

The TAM should work with the SA to prepare the [TAM kickoff deck](https://docs.google.com/presentation/d/1JfMfma2IO1OV12QWoNxmIrR0irE7piQP9nUdAjDO2Wk/edit#slide=id.g20a53d489f_0_213) to be presented as part of the meeting. This will be starting point of the TAM's relationship with the customer, and it's important to convey what the TAM's role is, and the expectations from both sides.

Please [review the details for the TAM kickoff call](/handbook/customer-success/tam/onboarding/#kickoff-call) to learn more about how to conduct this meeting.

### Shifting responsibility from SA to TAM

Once the account is in post-sales, the TAM takes primary responsibility for guidance & best practices conversations, customer enablement, and product usage. Most responsibilities of the Solutions Architect will transition to the TAM at this point.

During customer onboarding, the SA may choose to stay engaged with the customer to help facilitate a seamless handoff and address any ongoing activities from the POV. This should be done in such a way as to allow the TAM to take over those activities from the SA, so all conversations with the customer on these items should include the TAM.

Once the account is fully transitioned to post-sales, the SA will be involved in the following activities:

- POV for an additional business unit or a license tier upgrade
- Professional Services scoping and discovery for a [Statement of Work](/handbook/customer-success/professional-services-engineering/working-with/#custom-services-sow-creation-and-approval)
- Support of GitLab Days and on-site evangelism of GitLab at customer sites

Please review [full details about the overlap between SAs and TAMs](/handbook/customer-success/#overlap-between-solution-architects-and-technical-account-managers).

The SA may also be involved, as needed and at the discretion of the SA & their manager, to support customer enablement, EBR delivery, product demo, or other customer-facing activities for which the TAM may ask for assistance.
